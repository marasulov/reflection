﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Xml.Serialization;
using LibraryF;
using Newtonsoft.Json;

namespace OtusReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            int iterationValue = 10000;
            F f = new F();
            
            Console.WriteLine("Serializiation begining");
            var start = Stopwatch.StartNew();
            Serializer serializer = new Serializer(",");
            
            for (int i = 0; i < iterationValue; i++)
            {
                serializer.ToCSV(f.Get());
            }

            start.Stop();

            Console.WriteLine($"Serializiation time is {start.ElapsedMilliseconds} ms");

            start.Restart();

            Serializer serializer1 = new Serializer(",");
            string objString = serializer1.ToCSV(f.Get());

            Console.WriteLine(objString);

            start.Stop();

            Console.WriteLine($"Execution time :{start.ElapsedTicks} ticks");

            start.Restart();

            for (int i = 0; i < iterationValue; i++)
            {
                serializer.FromCSV<F>(objString);
            }
            start.Stop();
            Console.WriteLine($"Deserializiation time is {start.ElapsedMilliseconds} ms");
            
            start.Restart();
            for (int i = 0; i < iterationValue; i++)
            {
                JsonConvert.SerializeObject(f.Get());
            }
            start.Stop();

            var res = JsonConvert.SerializeObject(f.Get());
            

            Console.WriteLine($"Serializiation time  from JSON is {start.ElapsedMilliseconds} ms");

            start.Restart();
            for (int i = 0; i < iterationValue; i++)
            {
                JsonConvert.DeserializeObject(res);
            }
            start.Stop();

            Console.WriteLine($"Deserializiation time  from JSON is {start.ElapsedMilliseconds} ms");

        }

    }
}
