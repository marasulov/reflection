﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using LibraryF;

namespace OtusReflection
{
    public class Serializer
    {
        private StringBuilder _stringBuilder = new StringBuilder();
        private BindingFlags _bindingFlags;

        public string separator { get; set; }

        public StringBuilder StringBuilder
        {
            get => _stringBuilder;
            set => _stringBuilder = value;
        }

        public Serializer(string separator, BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static)
        {
            this.separator = separator;
            _bindingFlags = bindingFlags;
        }


        public string ToCSV(object obj)
        {
            foreach (FieldInfo field in typeof(F).GetFields(_bindingFlags))
            {

                _stringBuilder.Append(field.Name).Append(separator).Append(field.GetValue(obj)).Append(separator);
            }

            return _stringBuilder.ToString();
        }

        public object FromCSV<T>(string csv) where T : new()
        {
            object myLibraryClass = new T();

            var fields = myLibraryClass.GetType().GetFields();

            List<string> objFields = csv.Split(separator).Where((x, i) => i % 2 != 0).ToList();
            int i = 0;

            foreach (var field in fields)
            {
                field.SetValue(myLibraryClass, int.Parse(objFields[i]));
                i++;
            }

            return myLibraryClass;
        }
    }
}
